<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>register client</title>
</head>
<body>

	<h1>Enregistrer une personne</h1>
	
	<br>
	
	<c:url value="PersonServlet" var="createLink">
		<c:param name="operation" value="create"/>
	</c:url>
	
	<form action="${createLink}" method="post">
	
		<input type="text" name="fname" placeholder="entrer un nom ... ">
		<input type="text" name="lname" placeholder="entrer un pr�nom ... ">
		<br>
		<br>
		<input type="text" name="numstreet" placeholder="num�ro de rue ... ">
		<input type="text" name="street" placeholder="rue ... ">
		<input type="text" name="city" placeholder="ville ... ">
		<br>
		<br>
		<input type="tel" name="tel" placeholder="telephone ... ">
		<input type="email" name="mail" placeholder="entrer un email ... ">
		<br>
		<br>
		<input type="submit">
		
	</form>
	
	<br>
	
	<c:url value="index.jsp" var="indexLink"/>
	<!-- session � detruire -->
	<a href="${indexLink}">retour � l'index</a>
	
</body>
</html>