<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>search client</title>
</head>
<body>
	
	<h1>Search client</h1>
	
	<c:url value="CommandServlet" var="searchLink"/>
	
	<form action="${searchLink}" method="get">
	
			<select name="operation" id="operation">
			    <option value="search">
			    	<input type="number" name="id" placeholder="entrer l'id ... ">
			    </option>
			</select>
	
		<input type="submit">
		
	</form>
	
	<c:url value="index.jsp" var="indexLink"/>
	<a href="${indexLink}">retour � l'index</a>
	
		
</body>
</html>