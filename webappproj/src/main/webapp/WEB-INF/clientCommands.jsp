<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="model.Command"%>

<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@page isELIgnored="false" %>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>


<c:set var="commands" value="${ commands }" />

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>client commands</title>
</head>
<body>


<h2>Commandes de <c:out value="${commands.get(0).getPerson().getLname()}"/> </h2>

	<table>

		<tr>
			<td>
			
				<c:forEach items="${commands}" var="c" varStatus="status">
				
					<br>
					n: <c:out value="${status.count}"/> -
					<c:out value="${c}"/>
					
					<br>
					
				</c:forEach>
				
			</td>
		</tr>
		
	</table>
	
	<br>

	<c:url value="index.jsp" var="indexLink"/>
	<a href="${indexLink}">retour � l'index</a>
	
</body>
</html>