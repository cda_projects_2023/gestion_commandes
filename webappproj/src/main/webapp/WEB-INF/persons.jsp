<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="model.Person"%>

<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@page isELIgnored="false" %>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<c:set var="persons" value="${ persons }" />

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" 
	  rel="stylesheet" 
	  integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" 
	  crossorigin="anonymous">
<title>liste de clients</title>
</head>
<body>


	<h2>Liste de clients : </h2>

	<table>

		<tr>
			<td>
			
				<c:forEach items="${persons}" var="p" >
				
					<br>
					id : <c:out value="${p.idPerson}"/> -
					nom : <c:out value="${p.getLname()}"/>
					
					
				</c:forEach>
				
			</td>
		</tr>
		
		
		
		
	</table>
	
	<br>
	
	<a href="index.jsp">retour  l'index</a>
	
     
</body>
</html>