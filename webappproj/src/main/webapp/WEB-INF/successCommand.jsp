<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>success command</title>
</head>
<body>

	<h1>Commande effectu�e !</h1>
	
	<c:url value="index.jsp" var="indexLink"/>
	<!-- session � detruire -->
	<a href="${indexLink}">retour � l'index</a>

</body>
</html>