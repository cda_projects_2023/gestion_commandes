<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@ page isELIgnored="false" %>

<c:set var="person" value="${person}"/>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>register command</title>
</head>
<body>

	<h1>Commande de : <c:out value="${person.getLname()}"/></h1>
	
	<br>
	
	<c:url value="CommandServlet" var="createLink">
		<c:param name="operation" value="create"/>
		<c:param name="id" value="${person.getIdPerson()}"/>
	</c:url>
	
	<form action="${createLink}" method="post">
	
		<h3>prix � pay�</h3>
		 <input type="number" name="price" placeholder="prix � payer du client ... ">
		 
		 <br>
    	 <br>
		 
		 <h3>type de paiement</h3>
    	<input type="radio"  name="paymode" value="1">visa
    	<input type="radio"  name="paymode" value="2">mastercard
    
    	<br>
    	<br>
    	
    	<h3>�tat du paiement</h3>
    	<input type="radio"  name="statepayment" value="1" >pas pay�
    	<input type="radio"  name="statepayment" value="2">pay�
    	
		 <br>
    	<br>
		 
		 <h3>mode de livraison</h3>
		 <input type="radio"  name="deliverymode" value="1">chrono
    	 <input type="radio"  name="deliverymode" value="2">classic
    	 
    	 <br>
    	 <br>
    	 
    	 <h3>�tat de livraison</h3>
		 <input type="radio"  name="statedelivery" value="1">en cours
    	 <input type="radio"  name="statedelivery" value="2">termin�e
    	 
		 <br>
    	 <br>
		 
		<input type="submit">
		
	</form>
	
	<c:url value="index.jsp" var="indexLink"/>
	<!-- session � detruire -->
	<a href="${indexLink}">d�connexion</a>
	
	<br>

</body>
</html>