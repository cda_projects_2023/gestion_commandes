<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>   
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" 
	  rel="stylesheet" 
	  integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" 
	  crossorigin="anonymous">
<title>main</title>
</head>

<body>
	
	<h1>Gestion de commandes</h1>
	
	<c:url value="registerPerson.jsp" var="registerLink"/>
	<a href="${registerLink}">enregistrer un client et commander</a>
		
	<br>
	
	<c:url value="PersonServlet" var="personServlet">
			<c:param name="operation" value="display"/>
	</c:url>
	<a href="${personServlet}">liste de clients</a>
	
	<br>
	
	<c:url value="login.jsp" var="login"/>
	<a href="${login}">connexion</a>
	
	<br>
	
	<c:url value="searchClient.jsp" var="searchClient"/>
	<a href="${searchClient}">voir les commandes d'un client</a>
	
	<br>
	
	
</body>
</html>