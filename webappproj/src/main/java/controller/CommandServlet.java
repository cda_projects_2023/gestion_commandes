package controller;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Command;
import model.StatePayment;
import service.CommandService;
import service.DeliveryModeService;
import service.PayModeService;
import service.PersonService;
import service.StateDeliveryService;
import service.StatePaymentService;


public class CommandServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static LocalDate localDate = LocalDate.now();
	private Command command;
	private CommandService commandService = new CommandService();

    public CommandServlet() { }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String operation = request.getParameter("operation");
		
		if( operation != null && operation.equals("search") ) {
			Integer id = Integer.parseInt(  request.getParameter("id") );
			
			List<Command> commands = commandService.findAllById(id);
			
			request.setAttribute("id", id);
			request.setAttribute("commands", commands);
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/clientCommands.jsp").forward(request, response);
		}
	}
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		PersonService personService = new PersonService();
		PayModeService payModeService = new PayModeService();
		StatePaymentService statePaymentService = new StatePaymentService();
		DeliveryModeService deliveryModeService = new DeliveryModeService();
		StateDeliveryService stateDeliveryService = new StateDeliveryService();
		
		
		String operation = request.getParameter("operation");
		
		if( operation != null && operation.equals("create") ) {
			System.out.println(operation + " in post");
			
			Integer id = Integer.parseInt(  request.getParameter("id") );
			Double price = Double.parseDouble(request.getParameter("price"));
			Integer payMode = Integer.parseInt( request.getParameter("paymode"));
			Integer statePayment = Integer.parseInt(  request.getParameter("statepayment"));
			Integer deliveryMode = Integer.parseInt(  request.getParameter("deliverymode"));
			Integer stateDelivery = Integer.parseInt(  request.getParameter("statedelivery"));

			command  = new Command(
					Date.valueOf(localDate),
					price,
					personService.findById(id),
					payModeService.findById(payMode),
					statePaymentService.findById(statePayment),
					deliveryModeService.findById(deliveryMode),
					stateDeliveryService.findById(stateDelivery)
				);
			
			commandService.create(command);
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/successCommand.jsp").forward(request, response);
			
			
		}
	}

}
