package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Person;
import service.PersonService;


public class PersonServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private Person person;
	private PersonService personService = new PersonService();

    public PersonServlet() {}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String operation = request.getParameter("operation");
		System.out.println(operation);
		
		if( operation != null && operation.equals("display") ) {
			List<Person> listPersons = personService.findAll();
			request.setAttribute("persons", listPersons);
			this.getServletContext().getRequestDispatcher("/WEB-INF/persons.jsp").forward(request, response);
		}
		
		if( operation != null && operation.equals("login") ) {
			
			HttpSession session = request.getSession();
			
			Integer id = Integer.parseInt( request.getParameter("id") );
			Person person = personService.findById(id);
			
			session.setAttribute("person", person);
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/registerCommand.jsp").forward(request, response);
		}
		
		if( operation != null && operation.equals("delete") ) {
			
			Integer id = Integer.parseInt( request.getParameter("id") );
			Person person = personService.findById(id);
			
			personService.delete(person);
			List<Person> listPersons = personService.findAll();
			request.setAttribute("persons", listPersons);
			this.getServletContext().getRequestDispatcher("/WEB-INF/persons.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String operation = request.getParameter("operation");

		if( operation != null && operation.equals("create") ) {
			System.out.println(operation);
			
			person = new Person(
					request.getParameter("fname"),
					request.getParameter("lname"),
					request.getParameter("tel"),
					request.getParameter("numstreet"),
					request.getParameter("street"),
					request.getParameter("city"),
					request.getParameter("mail")
				);
			
			personService.create(person);
			
			this.getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}
}

