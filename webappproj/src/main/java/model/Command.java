package model;

import java.sql.Date;

import javax.persistence.*;

@Entity @NamedQueries({
    		   @NamedQuery(name = "findAllById", query = "SELECT c FROM Command c WHERE c.person.idPerson = :idPerson")
})

public class Command {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCommand;
	
	private Date date;
	
	private Double price;
	
	@ManyToOne 
	@JoinColumn(name = "idPerson")
	private Person person;
	
	@OneToOne 
	@JoinColumn(name = "idPayMode")
	private PayMode payMode;
	
	@OneToOne
	@JoinColumn(name = "idStatePayment")
	private StatePayment statePayment;
	
	@OneToOne
	@JoinColumn(name = "idDeliveryMode")
	private DeliveryMode deliveryMode;
	
	@OneToOne
	@JoinColumn(name = "idStateDelivery")
	private StateDelivery stateDelivery;
	
	public Command() {}
	
	public Command(Date date, Double price, Person person, PayMode payMode, StatePayment statePayment,
			DeliveryMode deliveryMode, StateDelivery stateDelivery) {
		this.date = date;
		this.price = price;
		this.person = person;
		this.payMode = payMode;
		this.statePayment = statePayment;
		this.deliveryMode = deliveryMode;
		this.stateDelivery = stateDelivery;
	}
	
	public Integer getIdCommand() {
		return idCommand;
	}
	
	public void setIdCommand(Integer idCommand) {
		this.idCommand = idCommand;
	}
	
	
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public PayMode getPayMode() {
		return payMode;
	}
	public void setPayMode(PayMode payMode) {
		this.payMode = payMode;
	}
	public StatePayment getStatePayment() {
		return statePayment;
	}
	public void setStatePayment(StatePayment statePayment) {
		this.statePayment = statePayment;
	}
	public DeliveryMode getDeliveryMode() {
		return deliveryMode;
	}
	public void setDeliveryMode(DeliveryMode deliveryMode) {
		this.deliveryMode = deliveryMode;
	}
	public StateDelivery getStateDelivery() {
		return stateDelivery;
	}
	public void setStateDelivery(StateDelivery stateDelivery) {
		this.stateDelivery = stateDelivery;
	}
	@Override
	public String toString() {
		return "Command [idCommand=" + idCommand + ", date=" + date + ", price=" + price + ", payMode=" + payMode.getType()
				+ ", statePayment=" + statePayment.getState() + ", deliveryMode=" + deliveryMode.getType() + ", stateDelivery="
				+ stateDelivery.getState() + "]";
	}
	
	

	
}
