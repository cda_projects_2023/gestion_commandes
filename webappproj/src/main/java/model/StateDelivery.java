package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class StateDelivery {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idStateDelivery;
	
	private String state;

	public Integer getIdStateDelivery() {
		return idStateDelivery;
	}

	public void setIdStateDelivery(Integer idStateDelivery) {
		this.idStateDelivery = idStateDelivery;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}
