package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class PayMode {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPayMode;
	
	private String type;

	public Integer getIdPayMode() {
		return idPayMode;
	}

	public void setIdPayMode(Integer idPayMode) {
		this.idPayMode = idPayMode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "PayMode [idPayMode=" + idPayMode + ", type=" + type + "]";
	}
	
	
}
