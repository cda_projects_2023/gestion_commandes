package model;

import java.util.List;
import java.util.ArrayList;

import javax.persistence.*;

@Entity 
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPerson;
    
    @Column(unique = true)
    private String fname;
    
    private String lname;
    private String telephone;
    private String number;
	private String street;
	private String city;
	
    @Column(unique = true)
    private String email;
    
    @OneToMany(mappedBy = "person", cascade = CascadeType.REMOVE)
    private List<Command> commands = new ArrayList<>();
    
    public Person() {}
    
	
	
	public Person(String fname, String lname, String telephone, String number, String street, String city, String email) {
		super();
		this.fname = fname;
		this.lname = lname;
		this.telephone = telephone;
		this.number = number;
		this.street = street;
		this.city = city;
		this.email = email;
	}



	public Integer getIdPerson() {
		return idPerson;
	}


	public void setIdPerson(Integer idPerson) {
		this.idPerson = idPerson;
	}


	public String getFname() {
		return fname;
	}


	public void setFname(String fname) {
		this.fname = fname;
	}


	public String getLname() {
		return lname;
	}


	public void setLname(String lname) {
		this.lname = lname;
	}


	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	@Override
	public String toString() {
		return System.lineSeparator() + "Person : "
			 + System.lineSeparator() + "idPerson=" + idPerson 
			 + System.lineSeparator() + ", fname=" + fname 
			 + System.lineSeparator() + ", lname=" + lname 
			 + System.lineSeparator() + ", numero de rue=" + number
			 + System.lineSeparator() + ", rue=" + street
			 + System.lineSeparator() + ", ville=" + city
			 + System.lineSeparator() + ", telephone=" + telephone 
			 + System.lineSeparator() + ", email=" + email
			 + System.lineSeparator();
	}
}

