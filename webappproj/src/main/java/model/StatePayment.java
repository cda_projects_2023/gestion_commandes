package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class StatePayment {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idStatePayment;
	
	private String state;
	
	public Integer getIdStatePayment() {
		return idStatePayment;
	}

	public void setIdStatePayment(Integer idStatePayment) {
		this.idStatePayment = idStatePayment;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "StatePayment [idStatePayment=" + idStatePayment + ", state=" + state + "]";
	}
	
	

}
