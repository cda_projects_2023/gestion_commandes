package service;

import java.util.List;

import dao.extension.StatePaymentDao;
import model.StatePayment;

public class StatePaymentService {
	 private final StatePaymentDao statePaymentDao = new StatePaymentDao();
	    
	    public void create( StatePayment statePayment){
	    	statePaymentDao.create(statePayment);
	    }

	    public void delete( StatePayment statePayment){
	    	statePaymentDao.delete(statePayment);
	    }

	    public void update( StatePayment statePayment){
	    	statePaymentDao.update(statePayment);
	    }

	    public  StatePayment findById(Integer id){
	        return statePaymentDao.findById(id);
	    }

	    public List< StatePayment> findAll(){
	        return statePaymentDao.findAll();
	    }
}