package service;

import java.util.List;

import dao.extension.CommandDao;
import dao.namedQuery.CommandNQDao;
import model.Command;

public class CommandService {
    private final CommandDao commandDao = new CommandDao();
    private final CommandNQDao commandNQDao = new CommandNQDao();
    
    public void create(Command command){
    	commandDao.create(command);
    }

    public void delete(Command command){
    	commandDao.delete(command);
    }

    public void update(Command command){
    	commandDao.update(command);
    }

    public Command findById(Integer id){
        return commandDao.findById(id);
    }

    public List<Command> findAll(){
        return commandDao.findAll();
    }
    
    public List<Command> findAllById(Integer id){
    	return commandNQDao.findAllById(id);
    }

}