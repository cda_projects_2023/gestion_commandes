package service;

import java.util.List;

import dao.extension.StateDeliveryDao;
import model.StateDelivery;

public class StateDeliveryService {
	 private final StateDeliveryDao stateDeliveryDao = new StateDeliveryDao();
	    
	    public void create(StateDelivery stateDelivery){
	    	stateDeliveryDao.create(stateDelivery);
	    }

	    public void delete(StateDelivery stateDelivery){
	    	stateDeliveryDao.delete(stateDelivery);
	    }

	    public void update(StateDelivery stateDelivery){
	    	stateDeliveryDao.update(stateDelivery);
	    }

	    public StateDelivery findById(Integer id){
	        return stateDeliveryDao.findById(id);
	    }

	    public List<StateDelivery> findAll(){
	        return stateDeliveryDao.findAll();
	    }
}
