package service;

import java.util.List;

import dao.extension.PayModeDao;
import model.PayMode;

public class PayModeService {
	private final PayModeDao payModeDao = new PayModeDao();
	
	 public void create( PayMode payMode){
		 	payModeDao.create(payMode);
	    }

	    public void delete( PayMode payMode){
	    	payModeDao.delete(payMode);
	    }

	    public void update( PayMode payMode){
	    	payModeDao.update(payMode);
	    }

	    public  PayMode findById(Integer id){
	        return payModeDao.findById(id);
	    }

	    public List< PayMode> findAll(){
	        return payModeDao.findAll();
	    }

}
