package service;

import java.util.List;

import dao.extension.DeliveryModeDao;
import model.DeliveryMode;

public class DeliveryModeService {
	private final DeliveryModeDao deliveryModeDao = new DeliveryModeDao();
	
	public void create( DeliveryMode deliveryMode){
	 	deliveryModeDao.create(deliveryMode);
    }

    public void delete( DeliveryMode deliveryMode){
    	deliveryModeDao.delete(deliveryMode);
    }

    public void update( DeliveryMode deliveryMode){
    	deliveryModeDao.update(deliveryMode);
    }

    public  DeliveryMode findById(Integer id){
        return deliveryModeDao.findById(id);
    }

    public List< DeliveryMode> findAll(){
        return deliveryModeDao.findAll();
    }
}
