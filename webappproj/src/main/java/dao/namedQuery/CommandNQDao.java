package dao.namedQuery;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import model.Command;
import utilitary.HibernateUtilitary;

public class CommandNQDao {
	
	 static Session session = HibernateUtilitary.getSessionFactory().openSession();

	public List<Command> findAllById(Integer id) {
		
		Query query = session.getNamedQuery("findAllById");
        query.setParameter("idPerson", id);

        return (List<Command>) query.list();
	}

}
