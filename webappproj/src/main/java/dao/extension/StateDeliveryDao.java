package dao.extension;

import dao.ObjectDao;
import model.StateDelivery;

public class StateDeliveryDao extends ObjectDao<StateDelivery>{
	
	public StateDeliveryDao() {
		super(StateDelivery.class);
	}
}
