package dao.extension;

import dao.ObjectDao;
import model.StatePayment;

public class StatePaymentDao extends ObjectDao<StatePayment>{
	
	public StatePaymentDao() {
		super(StatePayment.class);
	}
}