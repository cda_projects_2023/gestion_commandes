package dao.extension;

import dao.ObjectDao;
import model.PayMode;

public class PayModeDao extends ObjectDao<PayMode>{
	
	public PayModeDao() {
		super(PayMode.class);
	}
}
