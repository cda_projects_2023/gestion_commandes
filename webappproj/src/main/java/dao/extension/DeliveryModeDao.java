package dao.extension;

import dao.ObjectDao;
import model.DeliveryMode;

public class DeliveryModeDao extends ObjectDao<DeliveryMode>{
	
	public DeliveryModeDao() {
		super(DeliveryMode.class);
	}
}
