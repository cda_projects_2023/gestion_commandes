package dao.extension;

import dao.ObjectDao;
import model.Command;

public class CommandDao extends ObjectDao<Command>{
	
	public CommandDao() {
		super(Command.class);
	}
}
